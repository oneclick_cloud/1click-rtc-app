/*
 * _clickIosSdk.h
 *  1clickIosSdk
 *
 *  Created by Ravindra on 17/06/14.
 *  Copyright (c) 2014 1click.io All rights reserved.
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the 1click.io.
 * 4. Neither the name of the 1click.io nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 
 * THIS SOFTWARE IS PROVIDED BY 1click.io ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL 1click.io BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@protocol clickNotificationDelegate<NSObject>
-(void) sendNotification:(NSNumber*)status msg:(NSString*) msg;
-(void) callWait;
-(void) callJoin;
-(void) callConnected;
-(void) callDisconnected;
-(void) remoteHangUp;
-(void) audioLoss:(NSNumber*)loss;
-(void) videoLoss:(NSNumber*)loss;
-(void) recordNotification:(NSNumber*)status msg:(NSString*) msg;
-(void) didLocalVideoStart;
-(void) didRemoteVideoStart;
@end

@interface _clickIosSdk : NSObject
-(_clickIosSdk *)initWithDelegate:(id<clickNotificationDelegate>)delegate email:(NSString *)email apiKey:(NSString *)apiKey;
-(void) joinRoom:(NSString *)roomName pin:(NSString *)pin callType:(NSString *)callType videoParams:(NSDictionary *)videoParams remoteView:(UIView*)remoteView localView:(UIView*)localView;
-(void) exitRoom;
-(void) createRoom:(NSString *) roomName;
-(void) createRoomWithSettings:(NSString*)roomName bitrate:(NSNumber*)bitrate frameRate:(NSString*)frameRate type:(NSString*)type size:(NSNumber*)size;
-(void) invite:(NSString *)roomName emailList:(NSString *)emailList;
-(void) recordCall:(NSString *)recordName;
-(BOOL) muteAudio:(BOOL)enable;
-(BOOL) muteVideo:(BOOL)enable;
-(void) hideLocalVideo:(BOOL)enable;
-(void) switchCamera;
-(void) rotateVideo;
-(void) callCustomerWithName:(NSString *)name email:(NSString *)email group:(NSString *)group callType:(NSString *)callType videoParams:(NSDictionary *)videoParams remoteView:(UIView*)remoteView localView:(UIView*)localView;
@end