/*
 *  OneClickViewController.m
 *  1ClickRTC
 *
 *  Created by Ravindra on 17/06/14.
 *  Copyright (c) 2014 1click.io All rights reserved.
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the 1click.io.
 * 4. Neither the name of the 1click.io nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 
 * THIS SOFTWARE IS PROVIDED BY 1click.io ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL 1click.io BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */


#import "OneClickViewController.h"

//Macro definition for DIALOG BOX
#define BOX_ID        100
#define INVITE_BOX    101
#define JOIN_BOX      102
#define CREATE_BOX    103
//Macro definition for text field
#define ROOM_FIELD           201
#define EMAIL_FIELD          202
int textFieldTags[] = {201, 202};
int textFiledTagsLen = 2;

#define LEFT_SPACE 60

BOOL muteAudio = YES;
BOOL muteVideo = YES;

@interface OneClickViewController ()
@property (strong, nonatomic) NSString *roomName;
@property (strong, nonatomic) NSString *currentRoom;
@property (strong, nonatomic) NSString *email;
@property (strong, nonatomic) NSString *customerName;
@property (strong, nonatomic) NSString *customerEmail;
@property(nonatomic, assign) UIInterfaceOrientation statusBarOrientation;

@end

@implementation OneClickViewController{
    _clickIosSdk *click;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        click = [[_clickIosSdk alloc] initWithDelegate:self
                                                 email:@"developer@turtleyogi.com"
                                                apiKey:@"8620c9c27007b6656137c0920c262d898b1d7cf2"];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.view setBackgroundColor: [UIColor colorWithRed:35.0/255.0  green:28.0/255.0 blue:20.0/255.0 alpha:1.0f]];
    // Do any additional setup after loading the view from its nib.
    self.statusBarOrientation = [UIApplication sharedApplication].statusBarOrientation;
    [self layUIElementsOnTop];
    [self.inviteButton setBackgroundImage:[UIImage imageNamed:@"invite-idle"] forState:UIControlStateNormal];
    [self.inviteButton setBackgroundImage:[UIImage imageNamed:@"invite-active"] forState:UIControlStateSelected];
    [self.joinButton setBackgroundImage:[UIImage imageNamed:@"join-room-idle"] forState:UIControlStateNormal];
    [self.joinButton setBackgroundImage:[UIImage imageNamed:@"join-room-active"] forState:UIControlStateSelected];
    [self.createConference setBackgroundImage:[UIImage imageNamed:@"create-room-idle"] forState:UIControlStateNormal];
    [self.createConference setBackgroundImage:[UIImage imageNamed:@"create-room-active"] forState:UIControlStateSelected];
    [self.micButton setBackgroundImage:[UIImage imageNamed:@"mic-active"] forState:UIControlStateNormal];
    [self.micButton setBackgroundImage:[UIImage imageNamed:@"mic-mute"] forState:UIControlStateSelected];
    [self.videoMuteButton setBackgroundImage:[UIImage imageNamed:@"video-active"] forState:UIControlStateNormal];
    [self.videoMuteButton setBackgroundImage:[UIImage imageNamed:@"video-mute"] forState:UIControlStateSelected];
    [self.switchCameraButton setBackgroundImage:[UIImage imageNamed:@"switch-camera-front"] forState:UIControlStateNormal];
    [self.switchCameraButton setBackgroundImage:[UIImage imageNamed:@"switch-camera-back"] forState:UIControlStateSelected];
    [self setBackgroundImage:self.callView];
    // add tap gesture for dismiss keyboard.
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    [self.callView addGestureRecognizer:tap];
}

-(void) setBackgroundImage:(UIView*) view{
    UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
    CGRect bounds;
    CGSize size;
    // for portrait mode during application launch stage,
    // I was getting 1024x768 dimension, hence following code
    float width = view.frame.size.width;
    float height = view.frame.size.height;
    if((UIInterfaceOrientationPortrait == orientation) || (UIInterfaceOrientationPortraitUpsideDown == orientation)){
        //1024 x 768
        if(width > height){
            // 768, 1024
            size = CGSizeMake(height, width);
            bounds = CGRectMake(0, 0, height, width);
        }else{
            // 768, 1024
            size = CGSizeMake(width, height);
            bounds = CGRectMake(0, 0, width, height);
        }
    } else {
        //landscape mode
        bounds = CGRectMake(0, 0, width, height);
        size = CGSizeMake(width, height);
    }
    UIGraphicsBeginImageContext(size);
    [[UIImage imageNamed:@"background-aurora"] drawInRect:bounds];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    view.backgroundColor = [UIColor colorWithPatternImage:image];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self createRoomUI:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(BOOL)shouldAutorotate{
    // TODO: UI on iphone need to be fixed for rotation
    // currently this is not our focus.
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad ){
        // signal the change in orientation
        if (self.statusBarOrientation !=
            [UIApplication sharedApplication].statusBarOrientation) {
            self.statusBarOrientation =
            [UIApplication sharedApplication].statusBarOrientation;
            [self repositionDialogBox];
            [self setBackgroundImage:self.callView];
            [click rotateVideo];
        }
        return YES;
    } else {
        return NO;
    }
}

// ---------------------------------------------------------------------------------------------------
// 1CLICK library calling function start
- (void) sendInvite:(id) sender{
    // check whether roomName and email has been entered
    // before sending invite and dismissing the view.
    if([self.roomName length] && [self.email length]){
        // 1click library function
        // NSLog(@"%@ %@", self.roomName, self.email);
        [click invite:self.roomName emailList:self.email];
        [self removeView:nil];
    }
}

- (void) enterRoom:(id) sender{
    if([self.roomName length]){
        //NSLog(@"%@", self.roomName);
        //enable call view
        [self hideCallView:NO];
        // 1click library function
        [click joinRoom:self.roomName pin:@"" callType:@"video" videoParams:nil remoteView:self.callView localView:self.localView];
        self.currentRoom = [self.roomName lowercaseString];
        [self removeView:nil];
    }
}

- (void) createRoom:(id) sender{
    if([self.roomName length]){
        // 1click library function
        // NSLog(@"%@", self.roomName);
        [click createRoom:self.roomName];
        [self removeView:nil];
    }
}

- (IBAction)endCall:(id)sender {
    // 1click library function
    [click exitRoom];
    [self hideCallView:YES];
}

- (IBAction)micMute:(id)sender {
    // 1click library function
    BOOL success = [click muteAudio:muteAudio];
    if(success) {
        self.micButton.selected = muteAudio;
        muteAudio = !muteAudio;
    }
}

- (IBAction)videoMute:(id)sender {
    BOOL success = [click muteVideo:muteVideo];
    if(success){
        self.videoMuteButton.selected = muteVideo;
        muteVideo = !muteVideo;
    }
}

- (IBAction)switchCamera:(id)sender {
    // 1click library function
    [click switchCamera];
}

//------------------------------------------------------------------------
#pragma clickNotificationDelegate methods

-(void) sendNotification:(NSNumber *)status msg:(NSString *)msg{
    NSString* title;
    if(status){
        //success notifications
        title = @"INFO";
    } else {
        //failure notifications
        title = @"ERROR";
    }
    [self removeView:nil];
    self.informationDialog.center = [self getCenter];
    self.informationDialog.layer.zPosition = 2.0f;
    self.informationTitle.text = title;
    self.informationMsg.text = msg;
    //NSLog(@"sendNotification Complete");
    [self.view addSubview:self.informationDialog];
}

-(void) callWait{
    _callStatus.text = [NSString stringWithFormat:@"Call waiting.Please invite participants at https://us.1click.io/webdash/index.html#/Room/%@.", self.currentRoom];
}

-(void) callJoin{
    _callStatus.text = @"Joining call.";
}

-(void) callConnected{
    _callStatus.text = @"Call connected.";
}

-(void) remoteHangUp{
    _callStatus.text = @"Other participant left the conference.";
    // remote participant left.
    self.micButton.selected = self.videoMuteButton.selected = NO;
    muteAudio = muteVideo = YES;
    //self.recordButton.selected = NO;
    self.localView.hidden = YES;
}

-(void) audioLoss:(NSNumber *)loss{
    //NSLog(@"audio loss reported %@", loss);
}

-(void) videoLoss:(NSNumber *)loss{
    //NSLog(@"video loss reported %@", loss);
}

-(void) didLocalVideoStart{
    //NSLog(@"local video started");
    self.localView.hidden = NO;
}

-(void) didRemoteVideoStart{
    //NSLog(@"remote video started");
    [self layUIElementsOnTop];
}

-(void) recordNotification:(NSNumber *)status msg:(NSString *)msg{
    NSString * title;
    if(status){
        //self.recordButton.selected = TRUE;
        title = @"INFO";
        //NSLog(@"recordSuccess called");
    } else {
        title = @"ERROR";
    }
    UIAlertView * alert = [[UIAlertView alloc] initWithTitle:title
                                                     message:msg
                                                    delegate:self
                                           cancelButtonTitle:@"OK"
                                           otherButtonTitles:nil];
    [alert show];
}

-(void) callDisconnected{
    _callStatus.text = @"Call disconnected.";
    self.localView.hidden = YES;
    //self.videoView.hidden = YES;
}

//------------------------------------------------------------------------------------

// COMMON UI ELEMENT adding function

-(void)layUIElementsOnTop{
    // remote video z position will be at 0
    // assign z position for all ui elements
    self.callView.layer.zPosition = 0;
    [self stackElementsOnTop:self.localView radius:0.0];
    [self stackElementsOnTop:self.controlView radius:30.0];
    [self stackElementsOnTop:self.labelView radius:10.0];
    [self stackElementsOnTop:self.endCallView radius:10.0];
    self.endCallView.userInteractionEnabled = self.endCallView.exclusiveTouch = YES;
    [self stackElementsOnTop:self.bottomPanelView radius:30.0];
    
    //INFORMATION DIALOG
    [self stackElementsOnTop:self.informationDialog radius:30.0];
    [self stackElementsOnTop:self.informationMsg radius:30.0];
    //[self stackElementsOnTop:self.informationTitle radius:10.0];
    [self stackElementsOnTop:self.closeButton radius:20.0];
}

-(void)stackElementsOnTop:(UIView *)view radius:(float) radius{
    // z position
    view.layer.zPosition = 1.0;
    // border radius
    [view.layer setCornerRadius:radius];
    // border
    [view.layer setBorderColor:[UIColor lightGrayColor].CGColor];
    [view.layer setBorderWidth:1.5f];
}

-(UILabel*) uiLabelWithTitle:(NSString*) title boxWidth:(int) boxWidth{
    // create ui label for title
    UILabel *label = [[UILabel alloc] init];
    [label setFrame:CGRectMake(10, 0,  boxWidth - 2 * 10, 40)];
    label.backgroundColor=[UIColor clearColor];
    label.textColor=[UIColor whiteColor];
    label.userInteractionEnabled=NO;
    label.textAlignment = NSTextAlignmentCenter;
    label.text= title;
    return label;
}

-(UITextField*) uiTextFieldWithPlaceHolder:(NSString*) placeHolder frame:(CGRect)frame tag:(int)tag{
    UITextField *textField = [[UITextField alloc] initWithFrame:frame];
    textField.tag = tag;
    textField.borderStyle = UITextBorderStyleRoundedRect;
    textField.font = [UIFont systemFontOfSize:15];
    textField.placeholder = placeHolder;
    textField.autocorrectionType = UITextAutocorrectionTypeNo;
    if(tag == EMAIL_FIELD){
        textField.keyboardType = UIKeyboardTypeEmailAddress;
    } else {
        textField.keyboardType = UIKeyboardTypeDefault;
    }
    textField.clearButtonMode = UITextFieldViewModeWhileEditing;
    textField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    textField.delegate = self;
    [textField addTarget:self
                  action:@selector(editingChanged:)
        forControlEvents:UIControlEventEditingChanged];
    return textField;
}

-(UIButton*) uiButtonWithTitle:(NSString *) title frame:(CGRect)frame selector:(SEL)target_function{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [button addTarget:self
               action:target_function//@selector(aMethod:)
       forControlEvents:UIControlEventTouchUpInside];
    [button setTitle:title forState:UIControlStateNormal];
    button.frame = frame;
    button.backgroundColor = [UIColor colorWithRed:0.0/255.0f green:0.0/255.0f blue:0.0/255.0f alpha:1];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    button.layer.cornerRadius = 20; // this value vary as per your desire
    button.clipsToBounds = YES;
    return button;
}

-(void) removeView:(id)sender{
    for (UIView *subView in self.view.subviews)
    {
        if (subView.tag > BOX_ID)
        {
            [subView removeFromSuperview];
        }
    }
    self.joinButton.selected = self.createConference.selected = self.inviteButton.selected = NO;
    self.roomName = @"";
    self.email = @"";
}

-(void) hideCallView:(BOOL) hide{
    self.controlView.hidden = hide;
    self.localView.hidden = hide;
    self.endCallView.hidden = hide;
    self.micButton.selected = NO;
    self.videoMuteButton.selected = NO;
    self.switchCameraButton.selected = NO;
}

- (CGPoint) getCenter{
    CGPoint origin;
    //NSLog(@"%f %f", self.view.frame.size.width, self.view.frame.size.height);
    if(self.statusBarOrientation == UIInterfaceOrientationLandscapeLeft || self.statusBarOrientation == UIInterfaceOrientationLandscapeRight){
        origin = CGPointMake(self.view.frame.size.height/2, self.view.frame.size.width/4);
    } else {
        origin = CGPointMake(self.view.frame.size.width/2, 3 * self.view.frame.size.height/8);
    }
    return origin;
}

- (void) repositionDialogBox{
    for (UIView *subView in self.view.subviews)
    {
        if (subView.tag == INVITE_BOX || subView.tag == JOIN_BOX || subView.tag == CREATE_BOX)
        {
            //NSLog(@"subview setting center");
            [subView setCenter:[self getCenter]];
        } else if(self.informationDialog == subView){
            [subView setCenter:[self getCenter]];
        }
    }
}

// ---------------------------------------------------------------------------------------------
// DIALOG BOX CREATION FUNCTION START

- (UIView *)createDialogBoxWithLabel:(NSString *)title tag:(int)tag{
    // if any information box exist remove it from view.
    [self onInformationClose:nil];
    int dialogBoxWidth = self.view.frame.size.width - (2 * LEFT_SPACE);
    UIView* view = [[UIView alloc] initWithFrame: CGRectMake(LEFT_SPACE, 2 * LEFT_SPACE, dialogBoxWidth, 240)];
    view.tag = tag;
    [view setBackgroundColor: [UIColor colorWithRed:85.0/255.0  green:85.0/255.0 blue:85.0/255.0 alpha:1.0f]];
    [view setCenter:[self getCenter]];
    UILabel* label = [self uiLabelWithTitle:title boxWidth:dialogBoxWidth];
    [view addSubview:label];
    // border radius
    [view.layer setCornerRadius:30.0f];
    
    // border
    [view.layer setBorderColor:[UIColor lightGrayColor].CGColor];
    [view.layer setBorderWidth:1.5f];
    
    // drop shadow
    [view.layer setShadowColor:[UIColor blackColor].CGColor];
    [view.layer setShadowOpacity:0.8];
    [view.layer setShadowRadius:3.0];
    [view.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
    return view;
}

- (IBAction)onInformationClose:(id)sender {
    //self.informationDialog.hidden = YES;
    //[];
    [self.informationDialog removeFromSuperview];
}

- (IBAction)inviteUI:(id)sender {  
    [self removeView:nil];
    self.inviteButton.selected = YES;
    int dialogBoxWidth = self.view.frame.size.width - (2 * LEFT_SPACE);
    UIView* inviteDialogBox = [self createDialogBoxWithLabel:@"Invite a Friend" tag:INVITE_BOX];
    CGRect roomFrame = CGRectMake(10, 50, dialogBoxWidth - (2 * 10), 40);
    UITextField* roomTextField = [self uiTextFieldWithPlaceHolder:@"Enter room name here." frame:roomFrame tag:ROOM_FIELD];
    roomTextField.returnKeyType = UIReturnKeySend;
    CGRect emailFrame = CGRectMake(10, 100, dialogBoxWidth - (2 * 10), 40);
    UITextField* emailTextField = [self uiTextFieldWithPlaceHolder:@"Enter email-id here." frame:emailFrame tag:EMAIL_FIELD];
    emailTextField.returnKeyType = UIReturnKeySend;
    CGRect cancelFrame = CGRectMake(80.0, 160.0, 160.0, 40.0);
    UIButton* cancelButton = [self uiButtonWithTitle:@"CANCEL" frame:cancelFrame selector:@selector(removeView:)];
    CGRect inviteFrame = CGRectMake(400.0, 160.0, 160.0, 40.0);
    UIButton* inviteButton = [self uiButtonWithTitle:@"INVITE" frame:inviteFrame selector:@selector(sendInvite:)];
    [inviteDialogBox addSubview:roomTextField];
    [inviteDialogBox addSubview:emailTextField];
    [inviteDialogBox addSubview:cancelButton];
    [inviteDialogBox addSubview:inviteButton];
    [self.view addSubview: inviteDialogBox];
}

- (IBAction)joinRoomUI:(id)sender {
    [self removeView:nil];
    self.joinButton.selected = YES;
    int dialogBoxWidth = self.view.frame.size.width - (2 * LEFT_SPACE);
    UIView* joinDialogBox = [self createDialogBoxWithLabel:@"Join Room" tag:JOIN_BOX];
    CGRect roomFrame = CGRectMake(10, 50, dialogBoxWidth - (2 * 10), 40);
    UITextField* textField = [self uiTextFieldWithPlaceHolder:@"Enter room name here." frame:roomFrame tag:ROOM_FIELD];
    textField.returnKeyType = UIReturnKeyJoin;
    CGRect cancelFrame = CGRectMake(80.0, 160.0, 160.0, 40.0);
    UIButton* cancelButton = [self uiButtonWithTitle:@"CANCEL" frame:cancelFrame selector:@selector(removeView:)];
    CGRect inviteFrame = CGRectMake(400.0, 160.0, 160.0, 40.0);
    UIButton* inviteButton = [self uiButtonWithTitle:@"JOIN" frame:inviteFrame selector:@selector(enterRoom:)];
    [joinDialogBox addSubview:textField];
    [joinDialogBox addSubview:cancelButton];
    [joinDialogBox addSubview:inviteButton];
    [self.view addSubview: joinDialogBox];
}

- (IBAction)createRoomUI:(id)sender {
    [self removeView:nil];
    self.createConference.selected = YES;
    int dialogBoxWidth = self.view.frame.size.width - (2 * LEFT_SPACE);
    UIView* createConfDialogBox = [self createDialogBoxWithLabel:@"Create Room" tag:CREATE_BOX];
    CGRect roomFrame = CGRectMake(10, 50, dialogBoxWidth - (2 * 10), 40);
    UITextField* textField = [self uiTextFieldWithPlaceHolder:@"Enter room name here." frame:roomFrame tag:ROOM_FIELD];
    textField.returnKeyType = UIReturnKeyDone;
    CGRect cancelFrame = CGRectMake(80.0, 160.0, 160.0, 40.0);
    UIButton* cancelButton = [self uiButtonWithTitle:@"CANCEL" frame:cancelFrame selector:@selector(removeView:)];
    CGRect inviteFrame = CGRectMake(400.0, 160.0, 160.0, 40.0);
    UIButton* inviteButton = [self uiButtonWithTitle:@"CREATE" frame:inviteFrame selector:@selector(createRoom:)];
    [createConfDialogBox addSubview:textField];
    [createConfDialogBox addSubview:cancelButton];
    [createConfDialogBox addSubview:inviteButton];
    [self.view addSubview: createConfDialogBox];
}

-(void)dismissKeyboard {
    // on touch dismiss keyboard
    // we don't which keyboard is active iterate over
    // and dismiss all keyboard.
    for(int i=0; i < textFiledTagsLen; i++){
        UITextField *textField = (UITextField *)[self.view viewWithTag:textFieldTags[i]];
        [textField resignFirstResponder];
    }
}

//-----------------------------------------------------------------------------------------
#pragma mark - UITextFieldDelegate

- (void)textFieldDidEndEditing:(UITextField *)textField {
    [self editingChanged:textField];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    // There is no other control that can take focus, so manually resign focus
    // when return (Join) is pressed to trigger |textFieldDidEndEditing|.
    for (UIView *subView in self.view.subviews)
    {
        if (subView.tag == INVITE_BOX)
        {
            [self sendInvite:nil];
        } else if(subView.tag == JOIN_BOX){
            [self enterRoom:nil];
        } else if(subView.tag == CREATE_BOX){
            [self createRoom:nil];
        }
    }
    [textField resignFirstResponder];
    return YES;
}

- (void) editingChanged:(UITextField*)textField {
    if(textField.tag == ROOM_FIELD){
        self.roomName = textField.text;
    } else if(textField.tag == EMAIL_FIELD){
        self.email = textField.text;
    }
}

@end
