/*
 *  OneClickViewController.h
 *  1ClickRTC
 *
 *  Created by Ravindra on 17/06/14.
 *  Copyright (c) 2014 1click.io All rights reserved.
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the 1click.io.
 * 4. Neither the name of the 1click.io nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 
 * THIS SOFTWARE IS PROVIDED BY 1click.io ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL 1click.io BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#import <UIKit/UIKit.h>
#import "_clickIosSdk1_1.h"

@interface OneClickViewController : UIViewController<UITextFieldDelegate,
                                                     clickNotificationDelegate>
@property (strong, nonatomic) IBOutlet UIView *endCallView;
@property (strong, nonatomic) IBOutlet UIView *labelView;
@property (strong, nonatomic) IBOutlet UIView *controlView;
@property (strong, nonatomic) IBOutlet UIView *bottomPanelView;
@property (strong, nonatomic) IBOutlet UIButton *inviteButton;
@property (strong, nonatomic) IBOutlet UIButton *joinButton;
@property (strong, nonatomic) IBOutlet UIButton *createConference;
@property (strong, nonatomic) IBOutlet UIView *callView;
@property (strong, nonatomic) IBOutlet UIButton *micButton;
@property (strong, nonatomic) IBOutlet UIButton *videoMuteButton;
@property (strong, nonatomic) IBOutlet UIButton *switchCameraButton;
@property (strong, nonatomic) IBOutlet UILabel *callStatus;
@property (strong, nonatomic) IBOutlet UIView *localView;

@property (strong, nonatomic) IBOutlet UIView *informationDialog;
@property (strong, nonatomic) IBOutlet UILabel *informationTitle;
@property (strong, nonatomic) IBOutlet UILabel *informationMsg;
@property (strong, nonatomic) IBOutlet UIButton *closeButton;
- (IBAction)onInformationClose:(id)sender;

- (IBAction)inviteUI:(id)sender;
- (IBAction)joinRoomUI:(id)sender;
- (IBAction)createRoomUI:(id)sender;
- (IBAction)endCall:(id)sender;
- (IBAction)micMute:(id)sender;
- (IBAction)videoMute:(id)sender;
- (IBAction)switchCamera:(id)sender;
@end
