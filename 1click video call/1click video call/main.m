//
//  main.m
//  1click video call
//
//  Created by Ravindra on 25/08/14.
//  Copyright (c) 2014 1click. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
